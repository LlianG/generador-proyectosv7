import { environment } from "./environments/environment";

export const APP: any = {
    ApiEndpoint: environment.urlMaster,
    AppBaseUrl: environment.urlApp,
}
