import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP } from './constant';
import { DepartamentoClass } from './departamentos.class'

@Injectable({
    providedIn: 'root'
})
export class DepartamentosService {
    apiEndpoint = APP.ApiEndpoint;
    AppBaseUrl = APP.AppBaseUrl;
    constructor(
        private http: HttpClient,
    ) { }

    async listarConsecutivo(): Promise<any>{
        const url = `${this.apiEndpoint}/api/departamentos/consecutivo`;
        return await this.http.get<any>(url).toPromise();
    }

    async listarDepartamentos(data: DepartamentoClass): Promise<any>{
        const url = `${this.apiEndpoint}/api/departamentos/listar`;
        return await this.http.post<any>(url, data).toPromise();
    }

    async guardar_departamento(data: DepartamentoClass): Promise<any>{
        const url = `${this.apiEndpoint}/api/departamentos/guardar`;
        return await this.http.post<any>(url, data).toPromise();
    }
    async actualizar_departamento(data: DepartamentoClass): Promise<any>{
        const url = `${this.apiEndpoint}/api/departamentos/actualizar`;
        return await this.http.post<any>(url, data).toPromise();
    }
    async cambiar_estado(data: DepartamentoClass): Promise<any>{
        const url = `${this.apiEndpoint}/api/departamentos/cambiar_estado`;
        return await this.http.post<any>(url, data).toPromise();
    }

}
