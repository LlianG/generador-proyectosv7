import { NgModule } from '@angular/core';
import { DepartamentosFormComponent } from './departamentos.form.component';
import { HttpClientModule } from '@angular/common/http'
import { CommonModule } from '@angular/common'
import { FormsModule } from "@angular/forms";
import { DepartamentosService } from './departamentos.service'
import { SnotifyModule, ToastDefaults, SnotifyService } from "ng-snotify";
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { DynamicFormBuilderModule } from './dynamic-form-builder/dynamic-form-builder.module'

export const ToastConfig = Object.assign({}, ToastDefaults);
@NgModule({
  declarations: [DepartamentosFormComponent],
  imports: [
    HttpClientModule,
    CommonModule,
    FormsModule,
    NgxSmartModalModule.forRoot(),
    SnotifyModule,
    DynamicFormBuilderModule
  ],
  exports: [DepartamentosFormComponent, SnotifyModule, NgxSmartModalModule, DynamicFormBuilderModule],
  providers:[DepartamentosService,
    { provide: 'SnotifyToastConfig', useValue: ToastConfig},
    SnotifyService]
})
export class WeatherModule { }
