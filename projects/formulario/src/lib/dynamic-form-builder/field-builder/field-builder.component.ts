import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'field-builder',
  template: `
  <div class="form-group form-group-xs" [formGroup]="form">
    <label [attr.for]="field.label">
      {{field.label}}
      <strong class="text-danger" *ngIf="field.required">*</strong>
    </label>
    <div [ngSwitch]="field.type">
        <textbox  *ngSwitchCase="'text'" [field]="field" [form]="form"></textbox>
        <dropdown *ngSwitchCase="'dropdown'" [field]="field" [form]="form"></dropdown>
        <checkbox *ngSwitchCase="'checkbox'" [field]="field" [form]="form"></checkbox>
        <radio *ngSwitchCase="'radio'" [field]="field" [form]="form"></radio>
        <div *ngIf="!isValid && isDirty">
            <div class="text-required">
                ¡{{field.label}} es requerido!
            </div>
        </div>
    </div>
  </div>
  `
})
export class FieldBuilderComponent implements OnInit {
  @Input() field:any;
  @Input() form:any;
  
  get isValid() { return this.form.controls[this.field.name].valid;}
  get isDirty() { return this.form.controls[this.field.name].dirty;}

  constructor() { }

  ngOnInit() {
  }

}
