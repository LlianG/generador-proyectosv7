import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DISABLED } from '@angular/forms/src/model';
import { disableBindings } from '@angular/core/src/render3';

@Component({
  selector: 'dynamic-form-builder',
  template:`
    <form [formGroup]="form">
        <div class="modal-body">
            <div class="row">
                <field-builder *ngFor="let field of fields" [field]="field" [form]="form" [ngClass]="field.class"></field-builder>
            </div>
        </div>
        <div class="modal-footer">
            <button (click)="cancelar()" class="btn btn-default">Cerrar</button>
            <button (click)="guardar()" [disabled]="!form.valid" class="btn btn-primary" *ngIf="accion == 1">Guardar</button>
            <button (click)="guardar()" [disabled]="!form.valid" class="btn btn-primary" *ngIf="accion == 2">Editar</button>
        </div>
    </form>
  `,
})
export class DynamicFormBuilderComponent implements OnInit {
  val: any;
  form: FormGroup;
  @Output() onSubmit = new EventEmitter();
  @Output() onClose = new EventEmitter;
  @Input() fields: any[] = [];
  @Input() accion: number;
  @Input() set values(val: any){
    if(val){
      this.form.patchValue(val)
    }
  };
 
  
  constructor() { }

  ngOnInit() {
    let fieldsCtrls = {};
    for (let f of this.fields) {
      if (f.type != 'checkbox') {
        fieldsCtrls[f.name] = new FormControl({value: (f.value || ''), disabled: (f.disabled)?true:false}, Validators.required)
      } else {
        let opts = {};
        for (let opt of f.options) {
          opts[opt.key] = new FormControl(opt.value);
        }
        fieldsCtrls[f.name] = new FormGroup(opts)
      }
    }
    this.form = new FormGroup(fieldsCtrls);
    
  }
  cancelar(){
        this.form.reset()
        this.onClose.emit({ tipo: 1 });
  }
  guardar(){
        this.onSubmit.emit(this.form.value)
  }
}
