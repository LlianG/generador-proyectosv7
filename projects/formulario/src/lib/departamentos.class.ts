export class DepartamentoClass {
    constructor(
        public id?: number,
        public codigo?: string,
        public nombre?: string,
        public estado?: number,
        public accion?: string,
    ) {
        this.id = null;
        this.codigo = null;
        this.nombre = null;
        this.estado = null;
        this.accion = null;
    }
}
export class FormOptions {
    constructor(
        public EnableModal?: boolean

    ){
        this.EnableModal = true;
    }
}