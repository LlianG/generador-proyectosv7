import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormControl, NgForm, FormGroup } from '@angular/forms';
import { DepartamentoClass, FormOptions } from './departamentos.class';
import { DepartamentosService } from './departamentos.service';
import { SnotifyService } from 'ng-snotify';
import { ModalInstance } from 'ngx-smart-modal/src/services/modal-instance';


@Component({
    selector: 'ng-departamentos-form',
    templateUrl: 'departamentos.form.component.html',
    styleUrls: ['departamentos.form.component.css']
})

export class DepartamentosFormComponent implements OnInit {
    c_accion: number;
    formData: DepartamentoClass;
    option: FormOptions;
    htmlElement: string;    
    public form: FormGroup;
    unsubcribe: any
    defaultFields: any[] = [
        {
          type: 'text',
          name: 'codigo',
          class:'col-xs-4',
          label: 'Codigo',
          placeholder: 'Codigo',
          disabled: true,
          value: '',
          required: true,
        },
        {
          type: 'text',
          name: 'nombre',
          class:'col-xs-8',
          label: 'Nombre',
          placeholder: 'Nombre',
          disabled: false,
          value: '',
          required: true,
        }
    ];

    @Input() set fields(val: any[]){
        if(val.length > 0){
            this.defaultFields = val
        }
    }

    @Input() set accion(accion: number) {
        this.c_accion = accion;
        switch (accion) {
            case 1: //:: nuevo
                this.formData = new DepartamentoClass();
                this.consecutivo();
                this.Modal(1)
                break;
            case 2: //:: editar
                this.Modal(1)
                break;
        }
    }

    @Input() set idIn(id: number) {
        if (id) { //:: editar
            this.formData = new DepartamentoClass();
            this.departamentosService.listarDepartamentos({id, accion:'L'}).then(data => {
                this.formData = data.filas.recordset[0];
                // console.log(data)
            })
        }
    }

    @Input() set options(val: FormOptions){
        this.option = val;
    }

    @Input() values: any;

    @Output() close = new EventEmitter();

    @Output() onSubmit = new EventEmitter();


    constructor(
        public ngxSmartModalService: NgxSmartModalService,
        private snotifyService: SnotifyService,
        private departamentosService: DepartamentosService
    ) {
        this.formData = new DepartamentoClass();
        this.form = new FormGroup({
            fields: new FormControl(JSON.stringify(this.fields))
        })
        this.unsubcribe = this.form.valueChanges.subscribe((update) => {
            this.fields = JSON.parse(update.fields);
        });
    }

    ngOnInit() {
        //this.values = {codigo: '001', nombre: 'william', descripcion:'Hola'}
    }

    cerrar(e?) {
        this.Modal(2)
        this.close.emit({ tipo: 1 });
    }
    getFields() {
        return (this.fields.length>0)?this.fields:this.defaultFields;
    }
    async consecutivo(){
        await this.departamentosService.listarConsecutivo().then(result=>{
            this.formData.codigo = result.filas.recordset[0].consecutivo
        });
    }

    async guardar(form: NgForm) {
        this.snotifyService.async('Guardando departamento', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    this.formData.accion = 'G'
                    const data = Object.assign({}, this.formData);
                    await this.departamentosService.guardar_departamento(data);
                    form.reset();
                    resolve({
                        title: 'Exito',
                        body: 'Departamento guardado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 3000
                        }
                    });
                    this.Modal(2)
                    this.close.emit({ tipo: 2 });
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se puedo guardar la departamento',
                        config: { closeOnClick: true }
                    })
                }
            })
        );
    }

    async editar(form: NgForm) {
        this.snotifyService.async('Actualizando departamento', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    this.formData.accion = 'A'
                    const data = Object.assign({}, this.formData);
                    await this.departamentosService.actualizar_departamento(data);
                    form.reset();
                    resolve({
                        title: 'Exito',
                        body: 'Departamento actualizado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 3000
                        }
                    });
                    this.Modal(2)
                    this.close.emit({ tipo: 3 });
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se puedo actualizar el departamento',
                        config: { closeOnClick: true }
                    })
                }
            })
        );
    }
    ver(e){
        this.onSubmit.emit(e)
    }
    Modal(val: number){
        if(this.option.EnableModal){
            switch (val) {
                case 1:
                    this.ngxSmartModalService.getModal('departamentos_form_modal').open()
                    break;
                case 2:
                    this.ngxSmartModalService.getModal('departamentos_form_modal').close()
                    break;
                default:
                    break;
            } 
        }
        
    }
    ngDistroy() {
        this.unsubcribe();
    }

}
