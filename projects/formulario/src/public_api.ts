/*
 * Public API Surface of weather
 */
export * from './lib/departamentos.class';
export * from './lib/departamentos.service';
export * from './lib/departamentos.form.component';
export * from './lib/departamentos.module';
